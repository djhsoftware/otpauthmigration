﻿
namespace OtpAuthReader
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows;
    using System.Windows.Interop;
    using System.Windows.Media.Imaging;

    public class OtpAuthInfo : INotifyPropertyChanged
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        private BitmapSource bitmap;

        public BitmapSource Bitmap
        {
            get

            {
                return bitmap;
            }
            set
            {
                bitmap = value;
                OnPropertyChanged("Bitmap");
            }
        }

        public string Name { get; set; }
        public string Issuer { get; set; }
        public string Type { get; set; }

        public OtpAuthInfo(Bitmap bmp, string name, string issuer, string type, int width, int height)
        {
            bitmap = Bitmap2BitmapImage(bmp, width, height);
            Name = name;
            Issuer = issuer;
            Type = type;
        }

        private BitmapSource Bitmap2BitmapImage(Bitmap image, int width, int height)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(image.GetHbitmap(), IntPtr.Zero, System.Windows.Int32Rect.Empty, BitmapSizeOptions.FromWidthAndHeight(width, height));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
