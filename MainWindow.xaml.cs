﻿namespace OtpAuthReader
{
    using QRCoder;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// https://alexbakker.me/post/parsing-google-auth-export-qr-code.html
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void UriEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var bitmaps = Convert(UriEntry.Text);
                messageDisplay.Text = string.Format("Generated {0} QRCodes", bitmaps.Count);
                qrCodeView.ItemsSource = bitmaps;
            }
            catch (Exception ex)
            {
                messageDisplay.Text = ex.Message;
            }
        }

        /// <summary>
        /// Convert URI to Bitmap list
        /// </summary>
        /// <param name="uri">URI from Google backup</param>
        public List<OtpAuthInfo> Convert(string uri)
        {
            var data = uri.Replace("otpauth-migration://offline?data=", "");

            var decoded = WebUtility.UrlDecode(data);
            var converted = System.Convert.FromBase64String(decoded);
            var authData = Gauth.MigrationPayload.Parser.ParseFrom(converted);

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            var bitmaps = new List<OtpAuthInfo>();
            foreach (var record in authData.OtpParameters)
            {
                var codeData = string.Format("otpauth://totp/{0}?secret={1}&issuer={2}", record.Name, Base32Encode(record.Secret.ToByteArray()), record.Issuer);
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(codeData, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                bitmaps.Add(new OtpAuthInfo(qrCode.GetGraphic(20), record.Name, record.Issuer, record.Type.ToString(), 150, 150));
            }

            return bitmaps;
        }

        /// <summary>
        /// Base-32 Encode byte array
        /// </summary>
        /// <param name="data">Data to encode</param>
        /// <returns>Encoded Base32 string</returns>
        public string Base32Encode(byte[] data)
        {
            const int InByteSize = 8;
            const int OutByteSize = 5;
            const string Base32Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

            int i = 0, index = 0;
            var builder = new StringBuilder((data.Length + 7) * InByteSize / OutByteSize);

            while (i < data.Length)
            {
                int currentByte = data[i];
                int digit;

                // Is the current digit going to span a byte boundary?
                if (index > (InByteSize - OutByteSize))
                {
                    int nextByte;

                    if ((i + 1) < data.Length)
                    {
                        nextByte = data[i + 1];
                    }
                    else
                    {
                        nextByte = 0;
                    }

                    digit = currentByte & (0xFF >> index);
                    index = (index + OutByteSize) % InByteSize;
                    digit <<= index;
                    digit |= nextByte >> (InByteSize - index);
                    i++;
                }
                else
                {
                    digit = (currentByte >> (InByteSize - (index + OutByteSize))) & 0x1F;
                    index = (index + OutByteSize) % InByteSize;

                    if (index == 0)
                    {
                        i++;
                    }
                }

                builder.Append(Base32Alphabet[digit]);
            }

            return builder.ToString();
        }
    }
}
